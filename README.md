multipass
=========

![Multipass](http://1.bp.blogspot.com/_SWF9i3Vzpac/TM8K-A_0M1I/AAAAAAAAA_Q/s1E3WPIzSkc/s1600/Fifth%20Element%20Multipass.gif)

Multipass generator for use with desk.com authentication ([http://dev.desk.com/guides/sso/#what](http://dev.desk.com/guides/sso/#what)). Multipass authentication allows a logged in user to also be logged in to desk.com without requiring them to create an account on desk.com.

Example Usage
-------------

```python
from datetime import datetime, timedelta
from flask import current_app
from flask.ext.login import current_user
from multipass import MultipassGenerator


desk_url = 'https://support.canvasapps.io/'
expire_time = datetime.utcnow().replace(microsecond=0) + timedelta(minutes=10)
data = {
    'uid': current_user.email,
    'customer_email': current_user.email,
    'customer_name': current_user.name,
    'expires': expire_time.isoformat(),
    'to': desk_url,
}
multipass_generator = MultipassGenerator(
    current_app.config['DESK_SITE_KEY'], current_app.config['DESK_API_KEY'])
multipass, signature = multipass_generator.get_single_sign_on(data)

desk_login_url = '{desk_url}/customer/authentication/multipass/callback?multipass={multipass}&signature={signature}' \
    .format(desk_url=desk_url, multipass=urllib.quote(multipass), signature=urllib.quote(signature))
```

The `desk_login_url` can then be used to redirect the user to the support site and have them logged in automatically.


Errors
------

When installing the requirements for this package, `pycrypto`, you may receive this error:

```
clang: error: unknown argument: '-mno-fused-madd' [-Wunused-command-line-argument-hard-error-in-future]

clang: note: this will be a hard error (cannot be downgraded to a warning) in the future

error: command 'cc' failed with exit status 1
```

This can be rectified by exporting the following before installing requirements:

```
export CFLAGS=-Qunused-arguments
export CPPFLAGS=-Qunused-arguments
```
