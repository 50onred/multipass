from Crypto import Random
from Crypto.Cipher import AES
import base64
import hashlib
import hmac
import json


class MultipassGenerator:

    def __init__(self, site_key, api_key):
        self.site_key = site_key
        self.api_key = api_key

    def get_multipass(self, data):
        """ Generates a multipass for the given data. """
        json_data = data if isinstance(data, str) else json.dumps(data)
        salt = hashlib.sha1(self.api_key + self.site_key).digest()[:16]
        iv = Random.new().read(AES.block_size)
        aes = AES.new(salt, AES.MODE_CBC, iv)
        padded = json_data + (AES.block_size - len(json_data) % AES.block_size) * chr(AES.block_size - len(json_data) % AES.block_size)
        multi = aes.encrypt(padded)
        return base64.b64encode(iv + multi)

    def get_signature(self, multipass):
        """ Generates a signature for the given multipass. """
        signature = hmac.new(self.api_key, multipass, hashlib.sha1).digest()
        return base64.b64encode(signature)

    def get_single_sign_on(self, data):
        """ Gets the multipass & signature for the given data. """
        multipass = self.get_multipass(data)
        signature = self.get_signature(multipass)
        return multipass, signature