#!/usr/bin/env python
from setuptools import find_packages, setup

setup(
    name='multipass',
    version='0.2.0',
    url='https://bitbucket.org/50onred/multipass/overview',
    license='BSD',
    author='50onRed',
    author_email='devs@50onred.com',
    description='Multipass generator for desk.com',
    long_description=__doc__,
    zip_safe=False,
    include_package_data=True,
    packages=find_packages(),
    platforms='any',
    install_requires=[
        'pycrypto==2.6.1'
        ],
    classifiers=[
        'Environment :: Web Environment',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: BSD License',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Topic :: Internet :: WWW/HTTP :: Dynamic Content',
        'Topic :: Software Development :: Libraries :: Python Modules'
        ]
    )
